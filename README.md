# Esquisse de page de promotion Bénévalibre

Voici une esquisse de page d’accueil pour le projet *Bénévalibre*.

**Attention&nbsp;!** Cette page n’est pas la page officielle du projet *Bénévalibre* mais une simple proposition.

[Consulter le site web de *Bénévalibre*&nbsp;&rsaquo;](https://benevalibre.org)

## Logiciels libres utilisés

  * [Fork Awesome](https://forkaweso.me)
